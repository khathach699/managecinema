﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Manage_Cinema
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private Dictionary<string, decimal> seatPrices = new Dictionary<string, decimal>();

        private void Form1_Load(object sender, EventArgs e)
        {
             // Khởi tạo biến đếm với giá trị ban đầu là 13

            for (int i = 0; i < 13; i++)
            {
                int counter = 13 - i;
                for (int j = 0; j < 9; j++)
                {
                    Button btn = new Button();
                    btn.BackColor = System.Drawing.Color.White;
                    btn.FlatStyle = FlatStyle.Flat;
                    btn.FlatAppearance.BorderSize = 2;
                    btn.FlatAppearance.BorderColor = System.Drawing.Color.GreenYellow;
                    btn.Font = new System.Drawing.Font("Arial", 10);
                    btn.Name = (i * 13 + j + 1).ToString();
                    btn.Size = new System.Drawing.Size(60, 35);
                    btn.TabIndex = 1;

                    char rowChar = (char)('A' + j);
                    btn.Text = rowChar.ToString() + counter.ToString(); // Sử dụng giá trị của biến đếm
                    btn.UseVisualStyleBackColor = false;
                    btn.Location = new System.Drawing.Point(15 + i * 60, 0 + j * 32);

                    if (j >= 4)
                    {
                        btn.FlatAppearance.BorderSize = 2;
                        btn.FlatAppearance.BorderColor = System.Drawing.Color.Orange;
                    }
                    btn.Click += new EventHandler(btnChooseSeat);
                    panel1.Controls.Add(btn);
                    addbutton();

                    
                }
            }
        }

        private void btnChooseSeat(object sender, EventArgs e)
        {
             Button btn = sender as Button;
            if (btn.BackColor == System.Drawing.Color.White)
            {
                btn.BackColor = Color.SaddleBrown;
            }
            else if(btn.BackColor == System.Drawing.Color.SaddleBrown)
            {
                btn.BackColor = Color.White;
            }
            else if(btn.BackColor == System.Drawing.Color.Pink)
            {
                MessageBox.Show("The seat has been reserved");
            }
                
        }

        private void addbutton()
        {

            for (int i = 0; i < 15; i++)
            {
                int counter = 15 - i;
                {
                    Button btn = new Button();
                    btn.BackColor = System.Drawing.Color.Pink;
              
                    btn.Font = new System.Drawing.Font("Arial", 10);
                    btn.Name = (i * 13 ).ToString();
                    btn.Size = new System.Drawing.Size(50, 35);
                    btn.TabIndex = 1;

                    char rowChar = (char)('K');
                    btn.Text = rowChar.ToString() + counter.ToString(); // Sử dụng giá trị của biến đếm

                    btn.UseVisualStyleBackColor = false;
                    btn.Location = new System.Drawing.Point(0 + i * 55);
                    btn.Click += new EventHandler(btnChooseSeat);
                    panel2.Controls.Add(btn);
                    

                }
            }
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {

        }
    }
}

